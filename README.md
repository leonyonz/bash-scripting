bash-scripting
===============
## Apa itu bash

- BASH akronim dari **B**ourne **A**gain **S**hell.
- Bash salah satu nama unix shell
- Default dari mayoritas distro linux

## Varian Lain

- zsh - [Z Shell](https://ohmyz.sh/)
- ash - [Almquist shell](https://en.wikipedia.org/wiki/Almquist_shell)

## Bash Scripting

### Alasan menggunakan bash scripting.

-   Mengeliminasi task yang berulang-ulang.
-   Menghemat waktu.
-   Menyederhanakan command
-   Membuat flow (aliran) logika.
-   Digunakan di awal server (server start-up) atau dengan menambahkan cron job terjadwal.

### Ciri ciri

Selalu diawali dengan:

```bash
#!/bin/bash
```
**Note: Perhatikan karakter [ #! ]**
*Apakah gerangan maksudnya ?? :D*

### Sampel

**- Variable**

```bash
NAME="Tampan"
echo $NAME
or
echo "$NAME"
```

**- String Quotes**

```bash
NAME="John"
echo "Hi $NAME"  #=> Hi John
echo 'Hi $NAME'  #=> Hi $NAME
```

**- Interactive shell (basic)**

```bash
#!/bin/bash
read -p "Nama kamu siapa ganteng ? : " nama
echo "Nama saya $nama, cantik~"
```


### Easy ?????

**- Looping (Basic)**

```bash
echo {1..4}
or
echo {1,5,10}
or
echo {10..50..10}
```
**OR**
```bash
#!/bin/bash
for i in /etc/rc.*; do
  echo $i
done
```

**- Looping (C-Style)**

```bash
#!/bin/bash
for ((i = 0 ; i < 100 ; i++)); do
  echo $i
done
```

**Penerapan:**

```bash
for i in {1..4}; do ssh user@192.168.1.$i apt/yum update; done
```

## Still Easy!!

**- Array**

```bash
Fruits=('Apple' 'Banana' 'Orange')

or

Fruits[0]="Apple"
Fruits[1]="Banana"
Fruits[2]="Orange"
```

**- Pemanggilan Array**

```bash
echo ${Fruits[0]}           # Element #0
echo ${Fruits[-1]}          # Last element
echo ${Fruits[@]}           # All elements, space-separated
```

**- Case statement**

```bash
case $provinsi in

  1)
  echo ${jabar[@]}
  ;;

  2)
  echo ${jatim[@]}
  ;;

  3)
  echo ${jateng[@]}
  ;;

esac
```

## Let's Do IT!

```bash
#!/bin/bash

# Masuk ke directory testing
cd $PWD/testing

# Listing isi folder
c=1
for file in $( ls -1); do
    var[$c]="$file";
    c=$(($c+1));
done

# define variable
read -p "Karakter yang dicari : " old
read -p "Karakter yang mau diganti : " new

# list file
echo ""
echo "Berikut list file yang ada pada folder sampel:"
echo ""
for loop in "${var[@]}"; do
        echo $loop;
done
echo ""
read -p "Masukan nomor file : " numfile

# Fungsi
sed -i "s|$old|$new|g" $PWD/${var["$numfile"]}

# Verifikasi
cat $PWD/${var["$numfile"]}
```

```bash
#!/bin/bash

jabar=('bogor' 'bekasi' 'depok')
jatim=('jombang' 'malang' 'pasuruan')
jateng=('purwakerto' 'semarang' 'wonogiri')

echo "List provinsi !"

echo "1. jabar"
echo "2. jatim"
echo "3. jateng"

read -p "Masukan provinsi : " provinsi;

case $provinsi in

  1)
  echo ${jabar[@]}
  ;;

  2)
  echo ${jatim[@]}
  ;;

  3)
  echo ${jateng[@]}
  ;;

esac
```
